//
// Usage: node index.js [port_secure port_insecure host_name]
// 
var app = require('express')()
var bodyParser = require('body-parser')
var https = require('https')  
var http = require('http')
var clientSessions = require("client-sessions")
var os = require('os')
var fs = require('fs')
var db = require('/QOpenSys/QIBM/ProdData/Node/os400/db2i/lib/db2')
var config = require('./config') // get our config file
var xt = require('/QOpenSys/QIBM/ProdData/Node/os400/xstoolkit/lib/itoolkit')

//var option = {  xslib : "XMLSERVICE", ctl : "*debug" }
//var conn = new xt.iConn("*LOCAL", null, null, option)
var conn = new xt.iConn("*LOCAL")

var options = {
  key: fs.readFileSync('./ibmidash-key.pem'),
  cert: fs.readFileSync('./ibmidash-cert.pem')
}

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))

app.use(clientSessions({
	  secret: config.secret  
	}))

var port_secure = process.argv[2] || 58010
var port_insecure = process.argv[3] || 58000
var host_name = process.argv[4] || os.hostname()

app.locals._      = require('underscore')
app.locals._.str  = require('underscore.string')

//db.debug(true)
db.init()
db.conn("*LOCAL") 

app.set('views', __dirname + '/views')
app.set('view engine', 'jade')

app.get('/', function (req, res){
  if (req.session_state.username && req.session_state.password) {
    if (validateUser(req.session_state.username, req.session_state.password)) {
      res.send('Welcome ' + req.session_state.username + '! (<a href="/logout">logout</a>)')
    } else {
	  res.render('login')
    }
  } else {
    res.render('login')
  }
})

app.get('/secret', function (req, res){
  if (req.session_state.username && req.session_state.password) {
	if (validateUser(req.session_state.username, req.session_state.password)) {
	  res.render('secret')
	} else {
	  res.render('login')
    }
  } else {
	res.render('login')
  }
})

app.post('/loginsubmit', function (req, res){
  req.session_state.username = req.body.UserName
  req.session_state.password = req.body.Password
  if (validateUser(req.session_state.username, req.session_state.password)) {
    console.log(req.session_state.username + ' logged in.')
    res.redirect('/')
  } else {
	console.log(req.session_state.username + ' failed to login.')
	res.render('login')
  }  
})

app.get('/logout', function (req, res) {
  console.log(req.session_state.username + ' logged out.')
  req.session_state.reset()
  res.redirect('/')
})

 
http.createServer(function(req, res) {
  var new_loc = 'https://' + host_name + ':' + port_secure
  console.log('new_loc:%s', new_loc)
  res.writeHead(301,
    {Location: new_loc}
  )
  res.end()
}).listen(port_insecure)

var httpsServer = https.createServer(options, app).listen(port_secure)

// right padding s with c to a total of n chars
function padding_right(s, c, n) {
	  if (! s || ! c || s.length >= n) {
	    return s
	  }
	  var max = (n - s.length)/c.length
	  for (var i = 0; i < max; i++) {
	    s += c
	  }
	  return s
	}
	 
function validateUser(userName, password){
	  var pgm = new xt.iPgm("QSYGETPH",{"lib":"QSYS","error":"on"})
	  var x
	  console.log("entering validateUser()")
	  console.log(userName)
	  console.log(password)
	  pgm.addParam(padding_right(userName.toUpperCase(),' ',10), "10A")
	  pgm.addParam(padding_right(password.toUpperCase(),' ',10), "10A")   
	  pgm.addParam(" ", "12A", {"io":"out", "hex":"on"}) 
	  pgm.addParam([ 
		    [0,"10i0"],
		    [0,"10i0"],
		    [" ", "7A"],
		    [" ", "1A"],
		    [" ", "256A"]
	  ])
	  pgm.addParam(10, "10i0")
	  pgm.addParam(-1, "10i0")
	  conn.add(pgm.toXML())
	  conn.debug(true)
	  function my_call_back(str) {
		var results = xt.xmlToJson(str)
	    console.log(str)
	    console.log("entering my_call_back()")
	    if(results.length == 0){
	      console.log("success:true")
	      x = true
		} else {
		  results.forEach(function(result,index){
	        if(result.hasOwnProperty('success')){
	     	  console.log("success:" + result.success)
	     	  x = result.success
	        } else {
	          console.log("success:true")
	          x = true
	        }
		  })
		}
	  }
	  console.log("calling API")
	  conn.run(my_call_back)
	  console.log("exiting validateUser()")
	  return x
	}
