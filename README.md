# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is my test of the IBM i login page in node.js
* Version 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
    * Need to setup certificates following Aaron's article
    * Set your secret phrase in config.js
    * Set your default ports for http and https in index.js
* Dependencies
    * body-parser
    * client-sessions
    * express
    * jade
    * underscore
    * underscore.string
    * Node 4 (5733OPS option 5)
    * PTF Si61394
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Brian Garland (@BrianJGarland)