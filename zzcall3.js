var db = require('/QOpenSys/QIBM/ProdData/OPS/Node4/os400/db2i/lib/db2')
var xt = require('/QOpenSys/QIBM/ProdData/OPS/Node4/os400/xstoolkit/lib/itoolkit')
var conn = new xt.iConn("*LOCAL")

db.init()
db.conn("*LOCAL") 

// set the user and password to test
// the console will show "success:false" or "success:true"
validateUser("username","password")
	 
function validateUser(userName, password){
	  var pgm = new xt.iPgm("QSYGETPH",{"lib":"QSYS","error":"on"})
	  var x
	  
	  pgm.addParam(userName.toUpperCase(), "10A")
	  pgm.addParam(password.toUpperCase(), "10A")   
	  pgm.addParam(" ", "12A", {"io":"out", "hex":"on"}) 
	  pgm.addParam([ 
		    [0,"10i0"],
		    [0,"10i0"],
		    [" ", "7A"],
		    [" ", "1A"],
		    [" ", "256A"]
	  ])
	  pgm.addParam(10, "10i0")
	  pgm.addParam(-1, "10i0")
	  conn.add(pgm.toXML())
	  conn.debug(true)
	  function my_call_back(str) {
		var results = xt.xmlToJson(str)
		console.log(str)
	    if(results.length == 0){
	      console.log("success:true")
	      x = true
		} else {
		  results.forEach(function(result,index){
	        if(result.hasOwnProperty('success')){
	     	  console.log("success:" + result.success)
	     	  x = result.success
	        } else {
	          console.log("success:true")
	          x = true
	        }
		  })
		}
	  }
	  conn.run(my_call_back)
	  return x
	}
